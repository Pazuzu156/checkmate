#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,2,0,0
#define VER_FILEVERSION_STR         "2.2\0"

#define VER_PRODUCTVERSION          2,2,0,0
#define VER_PRODUCTVERSION_STR      "2.2\0"

#define VER_COMPANYNAME_STR         "Kaleb Klein"
#define VER_FILEDESCRIPTION_STR     "File Checksum Validator"
#define VER_INTERNALNAME_STR        "Checkmate"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (c) 2015 Kaleb Klein"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "Checkmate.exe"
#define VER_PRODUCTNAME_STR         "Checkmate"

#define VER_COMPANYDOMAIN_STR       "kalebklein.com"

#endif // VERSION_H

